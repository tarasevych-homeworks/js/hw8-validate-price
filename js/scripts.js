
let priceInput = document.getElementById('price');

priceInput.addEventListener("blur", () => onBlur());
priceInput.addEventListener("focus", () => onFocus());

function onBlur() {
    if(priceInput.value <= 0){
        document.getElementById('priceForm').insertAdjacentHTML('afterbegin',
            `<div class="currentPrice"><span>Please enter correct price</span></div>`);
        priceInput.classList.add('error');
        return;
    }

    document.getElementById('priceForm').insertAdjacentHTML('afterbegin', `
    <div class="currentPrice"><span>Текущая цена: ${priceInput.value} $ </span>
    <button class="deletePriceBtn">X</button></div>`);

    let price = document.querySelector('.currentPrice');
    document.querySelector('.deletePriceBtn').addEventListener("click",function() {
        price.remove();
        priceInput.value = '';
    });
}

function onFocus() {
    priceInput.classList.add('focus');
    document.querySelector('.currentPrice').remove();

    if(priceInput.classList.contains('error')){
        priceInput.classList.remove('error');
        document.querySelector('.currentPrice').remove();
    }

    if(priceInput.classList.contains('blur')){
        priceInput.classList.remove('blur');
    }
}

















// function changeStyle(n) {
//     if(priceInput.classList.contains(`${n}`)){
//         priceInput.classList.remove(`${n}`);
//     }
//     priceInput.classList.add(`${n}`);
// }









